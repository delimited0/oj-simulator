% SimpsonOJ
% simulate.m

%prices = zeros(4,12);

profit = 0;
purchase = zeros(4,12);
markets = zeros(7,4,12);
storagefacilities = zeros(sf,1);
processingcenters = zeros(pc, 1);
fcoj = 0;
for i = 1:12
    p = prices(:,i);
    run('totalop');
    profit = profit + cvx_optval;
    purchase(:,i) = x;
    markets(:,:,i) = q;
    storagefacilities = max(storagefacilities, caps);
    processingcenters = max(processingcenters, capp);
    fcoj = fcoj + sum(sum(c1f)) + sum(sum(c2f)) +sum(sum(c3f)) + ...
        sum(sum(c4f)) + sum(sum(c5f)) + sum(sum(c6f)) + sum(sum(c7f));
end
profit = profit - sf*7500000 - pc*8000000;